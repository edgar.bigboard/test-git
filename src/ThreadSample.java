import java.util.concurrent.TimeUnit;

public class ThreadSample {
    private static volatile boolean stopRequested;

    public static void main(String[] args) throws InterruptedException {
        int[] n1 = new int[0];
char[] ch = new char[20];

//        boolean[] n2 = new boolean[-200];
        System.out.println();

        Thread backgroundThread = new Thread(() -> {                                                    
            int i = 0;
            while (!stopRequested) {
//                System.out.println(i);
                i++;
            }

        });
        backgroundThread.start();

        TimeUnit.SECONDS.sleep(1);
        stopRequested = true;
        System.out.println("stop thread");

    }
}
